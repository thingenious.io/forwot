INSERT INTO ontology(id, domain, name, status, url, active)
VALUES (77, 'EnergyFOI', 'http://sensormeasurement.appspot.com/ont/m3/KoflerBuildingAutomation2011', 'OK',
        'https://www.auto.tuwien.ac.at/downloads/thinkhome/ontology/EnergyResourceOntology.owl', true);

INSERT INTO ontology(id, domain, name, status, url, active)
VALUES (188, 'BuildingAutomation', 'http://sensormeasurement.appspot.com/ont/m3/KoflerBuildingAutomation2011', 'OK',
        'https://www.auto.tuwien.ac.at/downloads/thinkhome/ontology/EnergyResourceOntology.owl', true);

INSERT INTO ontology(id, domain, name, status, url, active)
VALUES (189, 'BuildingAutomation', 'http://sensormeasurement.appspot.com/ont/m3/KoflerBuildingAutomation2011', 'OK',
        'https://www.auto.tuwien.ac.at/downloads/thinkhome/ontology/ActorOntology.owl', true);

INSERT INTO ontology(id, domain, name, status, url, active)
VALUES (200, 'EnergyFOI', 'http://sensormeasurement.appspot.com/ont/m3/Corrado2015EnergySEMANCOProject', 'OK',
        'http://semanco-tools.eu/ontology-releases/eu/semanco/ontology/SEMANCO/SEMANCO.owl?', true);

INSERT INTO ontology(id, domain, name, status, url, active)
VALUES (273, 'Transportation', 'http://sensormeasurement.appspot.com/ont/m3/LinkedGeoData2012Location', 'OK',
        'https://hobbitdata.informatik.uni-leipzig.de/LinkedGeoData/downloads.linkedgeodata.org/releases/2009-07-01/ontology.nt',
        true);

INSERT INTO ontology(id, domain, name, status, url, active)
VALUES (268, 'Transportation', 'http://sensormeasurement.appspot.com/ont/m3/Corsar2015transportdisruption', 'OK',
        'https://raw.githubusercontent.com/transportdisruption/transportdisruption.github.io/master/transportdisruption.owl',
        true);

INSERT INTO ontology(id, domain, name, status, url, active)
VALUES (297, 'Weather', 'http://sensormeasurement.appspot.com/ont/m3/KoflerBuildingAutomation2011', 'OK',
        'https://www.auto.tuwien.ac.at/downloads/thinkhome/ontology/ProcessOntology.owl', true);

INSERT INTO ontology(id, domain, name, status, url, active)
VALUES (299, 'Weather', 'http://sensormeasurement.appspot.com/ont/m3/KoflerBuildingAutomation2011', 'OK',
        'https://www.auto.tuwien.ac.at/downloads/thinkhome/ontology/EnergyResourceOntology.owl', true);

INSERT INTO ontology_entity(id, depth, description, domain, ontology_class, ontology_id, parent_class, active)
VALUES ('000311ea-7e25-4164-bbad-14487773c2d9', 3, 'Models the state of the object that illuminates', 'Weather',
        'LightIntensityState', '299', 'LevelState', true);

INSERT INTO ontology_entity(id, depth, description, domain, ontology_class, ontology_id, parent_class, active)
VALUES ('00177eca-f571-4891-9f3d-74a612897eb7', 1, null, 'Weather', 'SeasonalProfile', '297', 'Profile', true);

INSERT INTO ontology_entity(id, depth, description, domain, ontology_class, ontology_id, parent_class, active)
VALUES ('0004c272-958d-4447-a63e-27c6b292d6ce', 2, null, 'Transportation', 'wall', '273', 'building', true);

INSERT INTO ontology_entity(id, depth, description, domain, ontology_class, ontology_id, parent_class, active)
VALUES ('000aa323-5206-4aa3-928c-0a0cf23c620f', 4,
        'The bridge at the specified location has swung or lifted and is therefore temporarily closed to traffic. This concept and description are from the DATEX II concept bridgeSwingInOperation.',
        'Transportation', 'BridgeSwingInOperation', '268', 'GeneralNetworkManagement', true);

INSERT INTO ontology_entity(id, depth, description, domain, ontology_class, ontology_id, parent_class, active)
VALUES ('000707a2-55be-4c5f-857a-7bb44dc590f6', 0, 'Energy provider that solely provides energy through renewable energies.
', 'EnergyFOI', 'GreenEnergyProvider', '77', null, true);

INSERT INTO ontology_entity(id, depth, description, domain, ontology_class, ontology_id, parent_class, active)
VALUES ('000e8eb4-3650-4c5b-816e-eb6eff0e4ddf', 1,
        'angle on a horizontal plane between the normal to the surface and the north-south direction line', 'EnergyFOI',
        'Azimut_Angle', '200', 'AngleMeasure', true);

INSERT INTO ontology_entity(id, depth, description, domain, ontology_class, ontology_id, parent_class, active)
VALUES ('00103db1-e5f1-45e6-bc52-cde7933548c0', 0, 'GasEnergyDemand', 'BuildingAutomation', 'GasEnergyDemand', '188',
        null, true);

INSERT INTO ontology_entity(id, depth, description, domain, ontology_class, ontology_id, parent_class, active)
VALUES ('00264c6e-4047-4fe1-b6fc-501ee9d01c1d', 1,
        'this class was known as ''OccupancyPreferenceSchedule'' in prior versions of the ontology',
        'BuildingAutomation', 'OccupancyPreferenceSchedule', '189', 'PreferenceSchedule', true);

