import os
from app.internal.service.embeddings import EmbeddingsService

_ROOT_PATH = os.path.realpath(os.path.dirname(__file__))
_EMBEDDINGS_FILES_PATH = os.path.abspath(
    os.path.join(_ROOT_PATH, os.pardir, "embeddings")
)


def test_load_embeddings_from_file():
    # with open file throws error
    EmbeddingsService._EMBEDDINGS_DIRECTORY = _EMBEDDINGS_FILES_PATH
    loaded = EmbeddingsService._load_embeddings_from_file("Weather", "preprocessed")
    assert loaded is not None
