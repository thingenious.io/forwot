from typing import List
import os

import psycopg2
import pytest
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

from app.internal.models import OntologyEntity
from app.internal.transaction import OntologyEntityTransaction
from app.dependencies import DATABASE_URL
_ROOT_PATH = os.path.realpath(os.path.dirname(__file__))
_SQL_FILES_PATH = os.path.join(_ROOT_PATH, "..", "sql_files")


def initialize_db():
    conn = psycopg2.connect(DATABASE_URL.replace("+asyncpg", ""))
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = conn.cursor()
    sql_file_path = os.path.join(_SQL_FILES_PATH, "insert_ontologies_entities.sql")
    with open(sql_file_path, "r") as sql_file:
        # escaped_sql = sqlalchemy.text(sql_file.read())
        cursor.execute(sql_file.read())


@pytest.mark.asyncio
async def test_get_ontology_entities(init_database, db_session):
    initialize_db()
    results = await OntologyEntityTransaction.get_ontology_entities(db_session)
    assert len(results) == 8


@pytest.mark.asyncio
async def test_get_ontology_entities_by_domain_and_active(db_session):
    results_weather = (
        await OntologyEntityTransaction.get_ontology_entities_by_domain_and_active(
            db_session, "Weather"
        )
    )
    results_transportation = (
        await OntologyEntityTransaction.get_ontology_entities_by_domain_and_active(
            db_session, "Transportation"
        )
    )
    results_energyfoi = (
        await OntologyEntityTransaction.get_ontology_entities_by_domain_and_active(
            db_session, "EnergyFOI"
        )
    )
    results_building_automation = (
        await OntologyEntityTransaction.get_ontology_entities_by_domain_and_active(
            db_session, "BuildingAutomation"
        )
    )

    assert len(results_weather) == 2
    assert type(results_weather) == list
    assert is_only_one_domain("Weather", results_weather) is True

    assert len(results_transportation) == 2
    assert type(results_transportation) == list
    assert is_only_one_domain("Transportation", results_transportation) is True

    assert len(results_energyfoi) == 2
    assert type(results_energyfoi) == list
    assert is_only_one_domain("EnergyFOI", results_energyfoi) is True

    assert len(results_building_automation) == 2
    assert type(results_building_automation) == list
    assert is_only_one_domain("BuildingAutomation", results_building_automation) is True


def is_only_one_domain(domain: str, results: List[OntologyEntity]) -> bool:
    for ontology_entity in results:
        if ontology_entity.domain != domain:
            return False
    return True


@pytest.mark.asyncio
async def test_get_ontology_entities_by_domains(db_session):
    # results_ = await OntologyEntityTransaction.get_ontology_entities_by_domain_and_active
    pass
