"""-*- coding: utf-8 -*-."""
import asyncio
import os
import sys
from typing import List, Dict, Any, Generator

import pytest
import nltk  # noqa

# before starting the tests, we need to have a db running
# example:
# ../scripts/ci.sh:
# docker run -d --env POSTGRES_DB=test_db --name forwot-db-testing ....
# on ci we have these:
# DB_NAME=test_db
# DB_HOST=forwot-db-testing

try:
    from app.internal.models.base import Base
    from app.dependencies import async_session, engine, DATABASE_URL
except ImportError:
    sys.path.append(
        os.path.realpath(os.path.join(os.path.dirname(__file__), ".."))
    )  # noqa
    from app.internal.models.base import Base  # noqa
    from app.dependencies import async_session, engine, DATABASE_URL  # noqa


try:
    nltk.download("stopwords")
    nltk.download("punkt")
except FileExistsError:
    pass


@pytest.fixture()
def upper_split_test_cases():
    # type: () -> List[Dict[str,Any]]
    return [
        {"test_case": "HelloWorld", "result": True},
        {"test_case": "Helloorld", "result": True},
        {"test_case": "helloworld", "result": False},
    ]


@pytest.fixture()
def join_upper_test_cases():
    # type: () ->  List[Dict[str,List[str]]]
    return [
        {
            "test_case": ["word1", "word_2", "w3", "w"],
            "result": ["word1", "word_2", "w3", ""],
        },
        {
            "test_case": ["word1", "word_2", "w3", "word4"],
            "result": ["word1", "word_2", "w3", "word4"],
        },
    ]


@pytest.fixture(scope="session")
def get_ontology_class_list():
    # type: () -> List[Dict[str, str]]
    return [
        {"test_case": "BridgeSwingInOperation", "result": "bridge swing operation"},
        {"test_case": "crafts", "result": "crafts"},
        {"test_case": "Azimut_Angle", "result": "azimut angle"},
        {"test_case": "GasEnergyDemand", "result": "gas energy demand"},
        {"test_case": "Breakwater", "result": "breakwater"},
        {"test_case": "construction", "result": "construction"},
        {"test_case": "SeasonalProfile", "result": "seasonal profile"},
        {"test_case": "Sofa", "result": "sofa"},
        {"test_case": "route", "result": "route"},
        {"test_case": "SelectSourceCommand", "result": "select source command"},
        {"test_case": "tram_station", "result": "tram station"},
        {"test_case": "Conditioned_Gross_Volume", "result": "conditioned gross volume"},
        {"test_case": "Thermostat", "result": "thermostat"},
        {
            "test_case": "OccupancyPreferenceSchedule",
            "result": "occupancy preference schedule",
        },
        {"test_case": "ATC_Equipment", "result": "atc equipment"},
        {"test_case": "OffFlashingCommand", "result": "flashing command"},
        {"test_case": "Laboratory", "result": "laboratory"},
        {
            "test_case": "IdentificationFunctionality",
            "result": "identification functionality",
        },
        {"test_case": "DailyPowerTimeFrame", "result": "daily power time frame"},
        {"test_case": "WMBusComponent", "result": "wmbuscomponent"},
        {"test_case": "v_halt", "result": "halt"},
        {"test_case": "DDSkyClearnessHeat", "result": "ddskyclearnessheat"},
        {"test_case": "SaturdayPattern", "result": "saturday pattern"},
    ]


@pytest.fixture(scope="session")
def event_loop(request) -> Generator:
    """Create an instance of the default event loop for each test case."""
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
def event_loop(request) -> Generator:
    """Create an instance of the default event loop for each test case."""
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
def _database_url():
    return DATABASE_URL


@pytest.fixture(scope="session")
def init_database():
    return Base.metadata.create_all
