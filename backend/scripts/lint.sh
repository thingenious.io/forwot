#!/bin/sh

ROOT_DIR="$(dirname "$(dirname "$(readlink -f "$0")")")"

format_files() {
    pip install -r "${ROOT_DIR}/dev-requirements.txt" > /dev/null 2>&1 || :
    # Sort imports one per line, so autoflake can remove unused imports
    isort  --force-single-line-imports "${ROOT_DIR}"
    autoflake \
    --remove-all-unused-imports\
    --recursive\
    --remove-unused-variables\
    --in-place "${ROOT_DIR}"\
    --exclude=__init__.py
    black "${ROOT_DIR}"
    isort --settings-path "${ROOT_DIR}/.isort.cfg" "${ROOT_DIR}"
}

lint() {
    pip install -r "${ROOT_DIR}/dev-requirements.txt" > /dev/null 2>&1 || :
    mypy --config-file "${ROOT_DIR}/mypy.ini" "${ROOT_DIR}"
    black "${ROOT_DIR}"
    flake8 --config="${ROOT_DIR}/.flake8" "${ROOT_DIR}"
    if [ "${1}" = "--fix" ]; then
        format_files
    else
        isort --settings-path "${ROOT_DIR}/.isort.cfg" --check-only "${ROOT_DIR}"
    fi
}

if [ "${1}" = "--lint" ] || [ "${1}" = "-l" ] ; then
  shift
  lint "${@}"
fi

if [ "${1}" = "--format" ] || [ "${1}" = "-f" ] ; then
  format_files
fi
