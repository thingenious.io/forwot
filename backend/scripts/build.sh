#!/bin/sh

ROOT_DIR="$(dirname "$(dirname "$(readlink -f "$0")")")"

docker_build() {
  cd "${ROOT_DIR}" || exit
  if ! command -v docker > /dev/null 2>&1; then
    echo "docker command not found"
    exit 1
  fi
  USER_ID=${USER_ID:-$(id -u)}
  GROUP_ID=${GROUP_ID:-$(id -g)}
  TAG=${TAG:-latest}
  APP_ENV="${APP_ENV:-production}"
  docker build \
      --build-arg USER_ID="${USER_ID}" \
      --build-arg GROUP_ID="${GROUP_ID}" \
      --build-arg APP_ENV="${APP_ENV}" \
      -t "thingenious/forwot:${TAG}" .
}

if [ "${1}" = "-d" ] || [ "${1}" = "--docker" ]; then
  docker_build
fi
