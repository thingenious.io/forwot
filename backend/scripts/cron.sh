#!/bin/sh

if [ -f /home/runner/app/internal/service/ontologies_update.py ]; then
  # inside docker
  python /home/runner/app/internal/service/ontologies_update.py
else
  # relative to this file
  ROOT_DIR="$(dirname "$(dirname "$(readlink -f "$0")")")"
  if [ -f "${ROOT_DIR}/app/internal/service/ontologies_update.py" ]; then
    python "${ROOT_DIR}/app/internal/service/ontologies_update.py"
  else
    echo "could not find ontologies_update.py"
    exit 1
  fi
fi
