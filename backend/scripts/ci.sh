#!/bin/sh
# shellcheck disable=SC2086,SC2046

ROOT_DIR="$(dirname "$(dirname "$(readlink -f "$0")")")"

rm -rf "${ROOT_DIR}/coverage" > /dev/null 2>&1 || :
mkdir -p "${ROOT_DIR}/coverage" > /dev/null 2>&1 || :
chmod -R 0777 "${ROOT_DIR}/coverage" > /dev/null 2>&1 || :
export APP_ENV=development
export TAG=testing
sh "${ROOT_DIR}/scripts/build.sh" --docker
docker network create --attachable forwot-test-net --subnet=172.19.0.0/24 > /dev/null 2>&1 || :
# shellcheck disable=SC2046
docker run -d \
--name forwot-db-testing \
--env POSTGRES_USER=postgres \
--env POSTGRES_DB=test_db \
--env POSTGRES_PASSWORD=12345678 \
--expose 49154 \
--network=forwot-test-net \
postgres:12.2-alpine -p 49154
# wait for the service to restart after the db creation
sleep 10;
docker run --rm \
--name forwot-backend-testing \
-v ${ROOT_DIR}/tests:/home/runner/tests \
-v ${ROOT_DIR}/coverage:/home/runner/coverage \
--env APP_ENV=testing \
--env DB_NAME=test_db \
--env DB_HOST=forwot-db-testing \
--user $(id -u):$(id -g) \
--network=forwot-test-net \
thingenious/forwot:testing \
sh -c "/home/runner/forwotctl -t && rm -rf /home/runner/coverage/html"
sed -i "s|/home/runner/|backend/|" coverage/coverage.xml
sed -i "s|/home/runner/|backend/|" coverage/xunit.xml
docker stop forwot-db-testing > /dev/null 2>&1 || :
docker rm forwot-db-testing > /dev/null 2>&1 || :
docker network rm forwot-test-net > /dev/null 2>&1 || :
if [ ! "${1}" = "--skip-sonar" ]; then
    if command -v sonar-scanner > /dev/null 2>&1; then
        sonar-scanner
    fi
fi
rm -rf "${ROOT_DIR}/coverage/html" > /dev/null 2>&1 || :;
