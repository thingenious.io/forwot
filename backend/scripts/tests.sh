#!/bin/sh

ROOT_DIR="$(dirname "$(dirname "$(readlink -f "$0")")")"

backend_tests() {
  cd "${ROOT_DIR}" || exit
  rm -rf "${ROOT_DIR}/coverage" > /dev/null 2>&1 || :
  mkdir -p "${ROOT_DIR}/coverage" > /dev/null 2>&1 || :
  chmod -R 0777 "${ROOT_DIR}/coverage" > /dev/null 2>&1 || :
  pip install -r "${ROOT_DIR}/dev-requirements.txt" > /dev/null 2>&1 || :
  pip install -r "${ROOT_DIR}/requirements.txt" > /dev/null 2>&1 || :
  if [ ! "${1}" = "" ]; then
      # specify what to test
      # e.g. tests/test_lib.py::test_get_terms
      # also add extra args if needed
      pytest "${1}"
  else
    # separate tests (use --cov-append to collect):
    # app tests
    pytest -c "${ROOT_DIR}/pytest.ini"
  fi
}


backend_tests "${@}"
