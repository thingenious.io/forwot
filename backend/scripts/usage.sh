#!/bin/sh

_file=${1:-$(readlink -f "$0")}

actions() {
    printf "
\t-t|--test [FLAG]\t\tRun tests.
\t-s|--start [SERVICE]\t\tStart a SERVICE (--grpc|-g, -f|--frontend, default|empty: start fastAPI).
\t-o|--ontologies\t\t\tDownload and creat/update ontology entries.
\t-l|--lint [--fix]\t\tLint check (Optionally try to fix errors)
\t-b|--build\t\t\tBuild docker image
\t-i|--ci\t\t\t\tRun ci tests and code-analysis
\t-w|--wheel\t\t\tBuild lib/dist/forwot-{version}-py3-none-any.whl
\t-f|--format\t\t\tReformat files (in case of lint errors)
\t-n|--sonar\t\t\tRun sonar-scanner"
}

printf "\nUsage: %s ACTION [PARAM]
\nAvailable ACTIONS: %s
    \t-h|--help\t\t\tDisplay this message and exit\n\n" "${_file}" "$(actions)"
