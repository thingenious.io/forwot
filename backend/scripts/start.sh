#!/bin/sh

ROOT_DIR="$(dirname "$(dirname "$(readlink -f "$0")")")"

start_rest() {
  APP_ENV="${APP_ENV:-production}"
  REST_PORT=${REST_PORT-6080}
  cd "${ROOT_DIR}" || exit
  if [ "${APP_ENV}" = "development" ]; then
    uvicorn app.main:app --host 0.0.0.0 --port "${REST_PORT}" --reload
  else
    uvicorn app.main:app --host 0.0.0.0 --port "${REST_PORT}"
  fi
}

start_grpc () {
  APP_ENV="${APP_ENV:-production}"
  GRPC_PORT="${GRPC_PORT:-8061}"
  cd "${ROOT_DIR}" || exit
  python app/grpc_server.py
}


start_frontend() {
  cd frontend && yarn && yarn serve
}

case "${1}" in
  -f|--frontend)
    start_frontend
  ;;
  -g|--grpc)
    start_grpc
  ;;
  -r|--rest)
    start_rest
  ;;
  *)
    start_rest & start_grpc;
  ;;
esac
