# -*- coding: utf-8 -*-
from __future__ import absolute_import
import os
import uuid

import requests
import time
import logging
from datetime import datetime
from typing import Optional
import xml.etree.ElementTree as ET  # noqa

from pandas import DataFrame  # noqa
from psycopg2._psycopg import connection as psycopg_connection  # noqa
from requests import request
import psycopg2  # noqa
import psycopg2.extensions # noqa
from psycopg2.extras import register_uuid  # noqa
import ontospy  # noqa

try:
    from .preprocessor import Preprocessor
except ImportError:
    import sys

    sys.path.append(os.path.join(os.path.dirname(__file__)))
    from preprocessor import Preprocessor  # noqa


REQUEST_HEADERS = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) "
    "AppleWebKit/537.36 (KHTML, like Gecko) "
    "Chrome/83.0.4103.97 Safari/537.36"
}
BASE_URL = "https://lov4iot.appspot.com/perfectoOnto/getOntoDomain/?domain="
DOMAINS = ["BuildingAutomation", "EnergyFOI", "Transportation", "Weather", "Health"]
DB_HOST = os.environ.get("DB_HOST", "forwot-db")
DB_PORT = int(os.environ.get("DB_PORT", "49154"))
DB_NAME = os.environ.get("DB_NAME", "ai4eu-db")
DB_USER = os.environ.get("DB_USER", "postgres")
DB_PASSWORD = os.environ.get("DB_PASSWORD", "12345678")
REST_HOST = os.environ.get("REST_HOST", "http://forwot-backend")
EMBEDDINGS_DIR = os.environ.get(
    "EMBEDDINGS_DIR",
    os.path.realpath(
        os.path.join(os.path.dirname(__file__), "..", "..", "..", "embeddings")
    ),
)
REST_PORT = os.environ.get("REST_PORT", "8062")


def execute_update_transactions(connection, df):
    cursor = connection.cursor()
    for item in df.iterrows():
        _id = item[1][0]
        description = item[1][1]
        cursor.execute(
            "UPDATE ontology_entity SET description=(%s)"
            " WHERE id = (%s)",
            (description, _id),
        )
    connection.commit()


def fix_comments(df):
    # replace nan with ''
    df["description"] = df["description"].fillna("")

    # group by entity
    grouped = df.groupby("ontology_class")

    # find uniques
    uniques = df["ontology_class"].unique()

    # for every unique value in case there is a similar term with '' as description replace it with a non '' one
    for unique in uniques:
        c = grouped.get_group(unique).sort_values("description", ascending=False)
        text = c["description"].iloc[0]
        c["description"] = c["description"].apply(lambda x: text if x == "" else x)
        indices = c["description"].index
        df["description"].iloc[indices] = c["description"]
    return df


def update_ontologies_descriptions(connection):
    cursor = None
    try:
        cursor = connection.cursor()
        query = "SELECT id, description, ontology_class FROM public.ontology_entity"
        cursor.execute(query)
        ontologies_dataframe = DataFrame(
            cursor.fetchall(),
            columns=[
                "id",
                "description",
                "ontology_class",
            ],
        )
        ontologies_dataframe = fix_comments(ontologies_dataframe)
        execute_update_transactions(connection, ontologies_dataframe)

    except Exception as ex:
        logging.error(ex)
        connection.rollback()
    finally:
        if cursor is not None:
            cursor.close()


def save_entities_to_db(connection, entities_json):
    # type: (psycopg_connection, dict) -> Optional[uuid.UUID]
    """Save collected ontology entities to db."""
    register_uuid()
    ontology_entity_uuid = uuid.uuid4()
    preprocessed = Preprocessor.preprocess(entities_json["class"])
    active = entities_json.get("active", True)
    try:
        with connection.cursor() as cursor:
            cursor.execute(
                "INSERT INTO ontology_entity("
                "id, depth, description, domain, ontology_class, ontology_id, parent_class, active, preprocessed"
                ") VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                (
                    ontology_entity_uuid,
                    entities_json["depth"],
                    entities_json["description"],
                    entities_json["domain"],
                    entities_json["class"],
                    entities_json["ontology_id"],
                    entities_json["parent_class"],
                    active,
                    preprocessed,
                ),
            )
            connection.commit()
            return ontology_entity_uuid
    except Exception as ex:
        logging.error(ex)
        connection.rollback()
    return None


def get_subclasses(
    connection, ontology_class, parent_class, ontology_id, ontology_properties, depth
):
    # type: (psycopg_connection, ontospy, str,uuid.UUID, dict, int) -> None
    """Get subclasses of the selected ontology class."""
    _class = str(ontology_class.locale)
    if ".json" in _class:
        _class = _class.replace(".json", "")
    entity_to_save = {
        "class": _class,
        "parent_class": parent_class,
        "domain": ontology_properties["domain"],
        "ontology_id": ontology_id,
        "description": str(ontology_class.bestDescription()),
        "depth": depth,
        "active": True,
    }
    save_entities_to_db(connection=connection, entities_json=entity_to_save)
    if len(ontology_class.children()) == 0:
        return
    else:
        for oc in ontology_class.children():
            get_subclasses(
                ontology_class=oc,
                parent_class=_class,
                connection=connection,
                ontology_id=ontology_id,
                ontology_properties=ontology_properties,
                depth=depth + 1,
            )


def get_entities_by_ontology(connection, ontology_id, ontology_properties, model):
    # type: (psycopg_connection, uuid.UUID, dict, ontospy) -> None
    """Get ontology entities."""
    try:
        top_layer_classes = model.toplayer_classes
        initial_depth = 0

        for onto_class in top_layer_classes:
            parent_class = ""
            get_subclasses(
                connection=connection,
                ontology_class=onto_class,
                parent_class=parent_class,
                ontology_id=ontology_id,
                ontology_properties=ontology_properties,
                depth=initial_depth,
            )

    except Exception as ex:
        logging.error(ex)


def save_ontology_to_db(connection, ontology_properties):
    # type: (psycopg_connection, dict) -> Optional[uuid.UUID]
    """Save an ontology's details to db."""
    try:
        with connection.cursor() as cursor:
            cursor.execute(
                "INSERT INTO ontology(id, domain, name, url, status, active) "
                "VALUES (NEXTVAL('ontology_id_seq'), %s, %s, %s, %s, %s);",
                (
                    ontology_properties["domain"],
                    ontology_properties["name"],
                    ontology_properties["url"],
                    ontology_properties["status"],
                    ontology_properties["status"] == "OK",
                ),
            )
            cursor.execute("SELECT LASTVAL()")
            id_of_new_row = cursor.fetchone()[0]
        connection.commit()
        return id_of_new_row
    except Exception as ex:
        logging.error(ex)
        connection.rollback()
    return None


def get_ontology_info(ontology, status, domain):
    # type: (ET.Element, str, str) -> dict
    """Get the info of the parsed ontology."""
    ontology_name = ontology[0][0].text
    ontology_url = ontology[1][0].text

    return {
        "name": ontology_name,
        "url": ontology_url,
        "domain": domain,
        "status": status,
    }


def is_ontology_saved(connection, url):
    # type: (psycopg_connection, str) -> bool
    try:
        with connection.cursor() as cursor:
            query = "SELECT * FROM ontology WHERE url=%s"
            cursor.execute(query, (url,))
            results = cursor.fetchall()
            if len(results) > 0:
                return True
            else:
                return False
    except Exception as ex:
        logging.error(ex)
        return False


def get_ontology_request(connection, url, ontology, domain):
    # type: (psycopg_connection, str, ET.Element, str) -> str
    """Make a remote request to get ontology entities."""
    try:
        response = request("GET", url, headers=REQUEST_HEADERS, data={}, timeout=60)
        if response.status_code == 200:
            model = ontospy.Ontospy(data=response.content.decode().encode('utf-8'))
            ontology_info = get_ontology_info(ontology, "OK", domain)
            ontology_id = save_ontology_to_db(
                connection=connection, ontology_properties=ontology_info
            )
            if ontology_id:
                get_entities_by_ontology(
                    connection=connection,
                    ontology_id=ontology_id,
                    ontology_properties=ontology_info,
                    model=model,
                )
            return "OK"
        else:
            ontology_info = get_ontology_info(ontology, "NOT FOUND", domain)
            save_ontology_to_db(
                connection=connection, ontology_properties=ontology_info
            )
            return "NOT FOUND"
    except Exception as ex:
        logging.error(ex)
        ontology_info = get_ontology_info(ontology, "ERROR", domain)
        save_ontology_to_db(connection=connection, ontology_properties=ontology_info)
        return "ERROR"


def get_ontologies_by_domain_request(domain):
    # type: (str) -> ET.Element
    """Get the parsed ontology xml element for the specified domain."""
    url = BASE_URL + domain
    response = request("GET", url, headers={}, data={})
    return ET.fromstring(response.content.decode().encode('utf-8'))


def get_ontologies_by_domain(connection, domain):
    # type: (psycopg_connection, str) -> None
    """Download ontology entities by domain."""
    ontologies_content = get_ontologies_by_domain_request(domain)
    ontologies_length = len(ontologies_content[1])

    for ontology_index in range(0, ontologies_length):
        ontology = ontologies_content[1][ontology_index]
        url = ontology[1][0].text
        if url and not is_ontology_saved(connection, url):
            try:
                get_ontology_request(
                    connection=connection, url=url, ontology=ontology, domain=domain
                )
            except Exception as ex:
                logging.error(ex)


def delete_embeddings():
    for _file in os.listdir(EMBEDDINGS_DIR):
        os.remove(os.path.join(EMBEDDINGS_DIR, _file))


def recreate_embeddings():
    search_query = "search_term=loremipsum"
    for domain in DOMAINS:
        try:
            url = f"{REST_HOST}:{REST_PORT}/forwot/api/stats?domains={domain}"
            requests.get(url, timeout=180)
        except (requests.Timeout, Exception) as e:
            print(f"stats error: {e}")
        try:
            url = f"{REST_HOST}:{REST_PORT}/forwot/api/search-similarity?ontology_domain={domain}&{search_query}"
            requests.get(url, timeout=180)
        except (requests.Timeout, Exception) as e:
            print(f"search error: {e}")


def on_connection(connection):
    # type: (psycopg_connection) -> None
    """Create the tables in our db if not yet created, and get ontology entities."""
    create_tables = [
        "CREATE SEQUENCE IF NOT EXISTS ontology_id_seq",
        "CREATE TABLE IF NOT EXISTS public.ontology(id integer NOT NULL DEFAULT nextval('ontology_id_seq'),"  # noqa
        "domain character varying(255),"
        "name character varying(255),"
        "status character varying(255),"
        "url character varying(255),"
        "active boolean,"
        "CONSTRAINT ontology_pkey PRIMARY KEY (id)) WITH (OIDS = FALSE)",
        "ALTER SEQUENCE ontology_id_seq OWNED BY public.ontology.id",
        "CREATE TABLE IF NOT EXISTS public.ontology_entity(id uuid NOT NULL,"
        "depth integer,"
        "description text,"
        "domain character varying(255),"
        "ontology_class character varying(255),"
        "ontology_id integer,"
        "parent_class character varying(255),"
        "active boolean,"
        "preprocessed text NULL,"
        "FOREIGN KEY(ontology_id) REFERENCES ontology (id) ON DELETE CASCADE,"
        "CONSTRAINT ontology_entity_pkey PRIMARY KEY (id)) WITH (OIDS = FALSE)",
    ]

    with connection.cursor() as cursor:
        for item in create_tables:
            cursor.execute(item)

    connection.commit()

    for domain in DOMAINS:
        get_ontologies_by_domain(connection=connection, domain=domain)

    update_ontologies_descriptions(connection=connection)
    print("starting embeddings recreation...")
    delete_embeddings()
    recreate_embeddings()
    connection.close()


def main():
    # type: () -> None
    max_retries = 10
    delay = 5
    current = 0
    connection: Optional[psycopg_connection] = None
    time.sleep(delay)
    while current < max_retries:
        try:
            connection = psycopg2.connect(
                host=DB_HOST,
                port=DB_PORT,
                database=DB_NAME,
                user=DB_USER,
                password=DB_PASSWORD,
            )
            connection.set_client_encoding('UTF8')
            logging.info(f"Ready to continue, attempts: {current}")
            break
        except psycopg2.Error as error:
            logging.warning(f"Attempt {current + 1} of {max_retries}")
            if current == max_retries - 1:
                raise error
            time.sleep(delay)
        finally:
            current += 1
    if connection is not None:
        on_connection(connection=connection)


if __name__ == "__main__":
    print(f"started cron.py: {datetime.now()}")
    main()
    print(f"finished cron.py: {datetime.now()}")
