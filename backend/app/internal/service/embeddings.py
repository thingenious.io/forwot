import os
import pathlib
from typing import Optional, List

import numpy as np
import pandas  # noqa

from app.dependencies import torchserve_request


# The embeddings will be loaded from files. In case that they are not exist, will be retrieved by calling
# the embeddings microservices and will be stored locally in a file
class EmbeddingsService:
    _APP_ROOT = os.path.realpath(os.path.dirname(__file__))
    _EMBEDDINGS_DIRECTORY = os.path.realpath(
        os.path.join(_APP_ROOT, "..", "..", "..", "embeddings")
    )
    os.makedirs(_EMBEDDINGS_DIRECTORY, exist_ok=True)

    @staticmethod
    async def get_embedding_by_domains(
        domains: List[str],
        domain_dataframes: List[pandas.DataFrame],
        category: str,
        depth: Optional[int] = None,
    ) -> List[np.ndarray]:
        if category not in ["preprocessed", "description"]:
            raise Exception("Embedding category is not valid")
        else:
            embeddings = []
            for domain_index, domain in enumerate(domains):
                from_file = EmbeddingsService._load_embeddings_from_file(
                    domain, depth=depth, category=category
                )
                if from_file is not None:
                    embeddings.append(from_file)
                else:
                    from_request = await EmbeddingsService._get_embeddings_from_request(
                        domain=domain,
                        domain_dataframe=domain_dataframes[domain_index],
                        category=category,
                        depth=depth,
                    )
                    if from_request is not None:
                        embeddings.append(from_request)
            return embeddings

    @staticmethod
    def _load_embeddings_from_file(
        domain: str,
        category: str,
        depth: Optional[int] = None,
    ) -> Optional[np.ndarray]:
        filename = f"{domain}_{category}.npy"
        if depth is not None:
            filename = f"{domain}_{category}_depth{depth}.npy"
        file_path = os.path.join(EmbeddingsService._EMBEDDINGS_DIRECTORY, filename)
        lock_file = file_path.replace(".npy", ".lock")
        if os.path.exists(file_path) and os.path.exists(lock_file):
            return np.load(file_path)
        return None

    @staticmethod
    async def _get_embeddings_from_request(
        domain: str,
        domain_dataframe: pandas.DataFrame,
        category: str,
        depth: Optional[int] = None,
    ) -> Optional[np.ndarray]:
        filename = f"{domain}_{category}.npy"
        if depth is not None:
            filename = f"{domain}_{category}_depth{depth}.npy"
        lock_file = os.path.join(
            EmbeddingsService._EMBEDDINGS_DIRECTORY, filename.replace(".npy", ".lock")
        )
        force = not os.path.exists(lock_file)
        data = {
            "domain_name": domain,
            "terms": list(domain_dataframe[category]),
            "depth": depth,
            "force": force,
        }
        embeddings_ndarray = await torchserve_request(data=data, embeddings=True)
        if embeddings_ndarray is not None:
            EmbeddingsService._store_embeddings_to_file(
                embeddings=embeddings_ndarray, filename=filename
            )
        return embeddings_ndarray

    @staticmethod
    def _store_embeddings_to_file(embeddings: np.ndarray, filename: str) -> None:
        if embeddings is not None:
            file_path = os.path.realpath(os.path.join(EmbeddingsService._EMBEDDINGS_DIRECTORY, filename))
            lock_file = file_path.replace(".npy", ".lock")
            pathlib.Path.touch(pathlib.Path(lock_file), mode=0x777, exist_ok=True)
            np.save(file_path, embeddings)
        else:
            raise Exception("ndarray cant be None")

    @staticmethod
    def remove_stored_embeddings_by_domain(domain: str) -> None:
        files: List[str] = os.listdir(EmbeddingsService._EMBEDDINGS_DIRECTORY)
        for file in files:
            if domain in file:
                file_path = os.path.join(EmbeddingsService._EMBEDDINGS_DIRECTORY, file)
                os.remove(file_path)
