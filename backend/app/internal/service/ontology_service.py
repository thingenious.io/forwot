from typing import Optional, final, List, Union

import pandas as pd # noqa
from sqlalchemy.ext.asyncio import AsyncSession

from app.internal.models import Ontology # noqa
from app.internal.transaction.ontology_transaction import OntologyTransaction # noqa
from app.internal.service.embeddings import EmbeddingsService # noqa


@final
class OntologyService:
    @staticmethod
    async def get_ontologies_in_list(session):
        # type: (AsyncSession) -> List[Ontology]
        ontologies = await OntologyTransaction.get_ontologies(session)

        return ontologies

    @staticmethod
    async def get_unique_domains(session):
        # type: (AsyncSession) -> List[Union[str, Ontology]]
        ontologies = await OntologyTransaction.get_unique_domain_ontologies(session)
        domains = []
        for ontology in ontologies:
            if ontology.domain:
                domains.append(ontology.domain)

        return domains

    @staticmethod
    async def get_ontologies_by_domains_in_list(session, domains):
        # type: (AsyncSession, List[str]) -> List[Ontology]
        return await OntologyTransaction.get_ontologies_by_domains(session, domains)

    @staticmethod
    async def get_ontologies_in_dataframe(session):
        # type: (AsyncSession) -> pd.DataFrame
        query_result = await OntologyTransaction.get_ontologies(session)
        ontologies_dict = Ontology.to_dict(query_result)
        return pd.DataFrame.from_dict(ontologies_dict)

    @staticmethod
    async def blacklist_ontology(session, ontology_id):
        # type: (AsyncSession, int) -> None
        await OntologyTransaction.update_ontology_active_property(
            session, ontology_id, False
        )
        ontology: Optional[Ontology] = await OntologyTransaction.get_ontology_by_id(
            session, ontology_id
        )
        if ontology:
            EmbeddingsService.remove_stored_embeddings_by_domain(ontology.domain)

    @staticmethod
    async def whitelist_ontology(session, ontology_id):
        # type: (AsyncSession, int) -> None
        await OntologyTransaction.update_ontology_active_property(
            session, ontology_id, True
        )
        ontology: Optional[Ontology] = await OntologyTransaction.get_ontology_by_id(
            session, ontology_id
        )
        if ontology:
            EmbeddingsService.remove_stored_embeddings_by_domain(ontology.domain)
