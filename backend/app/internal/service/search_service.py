import os
import pathlib
from typing import final, Optional

import numpy as np  # noqa
import pandas as pd  # noqa
from sqlalchemy.ext.asyncio import AsyncSession


from app.internal.models import OntologyEntity, Ontology # noqa
from app.internal.transaction.ontology_entity_transaction import (  # noqa
    OntologyEntityTransaction,
)
from app.internal.transaction.ontology_transaction import OntologyTransaction # noqa
from app.dependencies import ( # noqa
    DEFAULT_BATCH_SIZE,
    DEFAULT_COSINE_THRESHOLD,
    DEFAULT_TRANSFORMER_THRESHOLD,
    DEFAULT_CONVERT_TERM_THRESHOLD,
    EMBEDDINGS_DIR,
    torchserve_request,
)


@final
class SearchService:
    @staticmethod
    def search_entity(key, ontologies_df, domain_df):
        # type: (str, pd.DataFrame, pd.DataFrame) -> pd.DataFrame
        """Search a key in the domain's dataframe.

        text: the entity the user searches for
        domain_df: the dataframe of a specific domain
        """
        entities_df = pd.DataFrame({"ontologies": [], "entities": []})
        string = key.lower()
        domain_df["lowered"] = domain_df["ontology_class"].str.lower()

        # iterate over the existing ontologies
        for domain_index in domain_df["ontology_id"].unique():
            indices = domain_df.index[domain_df["ontology_id"] == domain_index]
            same_entities = (
                domain_df["ontology_class"]
                .iloc[indices][domain_df["lowered"].iloc[indices].str.contains(string)]
                .values
            )
            if len(same_entities) > 0:
                ontologies = ontologies_df["url"][
                    ontologies_df["id"] == domain_index
                ].values[0]
                entities = pd.DataFrame(
                    {"ontologies": ontologies, "entities": same_entities}
                )
                entities_df = entities_df.append(entities, ignore_index=True)

        return entities_df

    @staticmethod
    async def search_term_in_domain(
        session,
        search_term,
        ontology_domain,
    ):
        # type: (AsyncSession, str, str) -> pd.DataFrame
        # Simple (deterministic) search of term in domain's entities.
        entities = (
            await OntologyEntityTransaction.get_ontology_entities_by_domain_and_active(
                session, ontology_domain, max_depth=None
            )
        )
        entities_dict = OntologyEntity.to_dict(entities)
        domain_df = pd.DataFrame(entities_dict)
        ontologies = await OntologyTransaction.get_ontologies(session)
        ontologies_dict = Ontology.to_dict(ontologies)
        ontologies_df = pd.DataFrame(ontologies_dict)
        results = SearchService.search_entity(
            key=search_term, ontologies_df=ontologies_df, domain_df=domain_df
        )
        return results

    @staticmethod
    async def get_search_similarity(
        session,
        search_term,
        ontology_domain,
        n_terms=DEFAULT_BATCH_SIZE,
        alpha=DEFAULT_COSINE_THRESHOLD,
        beta=DEFAULT_TRANSFORMER_THRESHOLD,
        gamma=DEFAULT_CONVERT_TERM_THRESHOLD,
        depth=None,
    ):
        # type: (AsyncSession, str, str, int, float, float, float, Optional[int]) -> Optional[pd.DataFrame]
        ontologies = await OntologyTransaction.get_ontologies(session)
        ontologies_dict = Ontology.to_dict(rows=ontologies)
        ontologies_df = pd.DataFrame(ontologies_dict)
        ontology_entities = (
            await OntologyEntityTransaction.get_ontology_entities_by_domain_and_active(
                session, ontology_domain, max_depth=None
            )
        )
        domain_dict = OntologyEntity.to_dict(rows=ontology_entities)
        domain_df = pd.DataFrame(domain_dict)
        filename = f"{ontology_domain}_preprocessed.npy"
        if depth is not None:
            filename = f"{ontology_domain}_preprocessed_depth{depth}.npy"
        lock_file = os.path.join(EMBEDDINGS_DIR, filename.replace(".npy", ".lock"))
        force = not os.path.exists(lock_file)
        data = {
            "domain_name": ontology_domain,
            "text": search_term,
            "n_terms": n_terms,
            "alpha": alpha,
            "beta": beta,
            "gamma": gamma,
            "ontologies_df": ontologies_df.to_json(),
            "domain_df": domain_df.to_json(),
            "depth": depth,
            "force": force,
        }
        print(
            {
                "domain_name": ontology_domain,
                "text": search_term,
                "n_terms": n_terms,
                "alpha": alpha,
                "beta": beta,
                "gamma": gamma,
                "ontologies_df.shape": ontologies_df.shape,
                "domain_df.shape": domain_df.shape,
                "depth": depth,
                "force": force,
            }
        )
        torchserve_response = await torchserve_request(data=data, embeddings=False)
        if torchserve_response is not None:
            pathlib.Path.touch(pathlib.Path(lock_file), mode=0x777, exist_ok=True)
            return pd.read_json(torchserve_response)
        raise ValueError("Could not get similarities")
