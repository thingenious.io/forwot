from typing import List, Optional, Union

from sqlalchemy import select, update, and_
from sqlalchemy.ext.asyncio import AsyncSession
from app.internal.models.ontology import Ontology


class OntologyTransaction:
    @staticmethod
    async def get_ontologies(session):
        # type: (AsyncSession) -> List[Ontology]
        result = await session.execute(select(Ontology))
        return result.scalars().all()

    @staticmethod
    async def get_unique_domain_ontologies(session):
        # type: (AsyncSession) -> List[Ontology]
        # we could probably just use: select(Ontology.domain) instead (i.e. only get that column)
        result = await session.execute(select(Ontology).distinct(Ontology.domain))
        return result.scalars().all()

    @staticmethod
    async def get_ontologies_by_domains(session, domains):
        # type: (AsyncSession, List[str]) -> List[Ontology]
        result = await session.execute(
            select(Ontology).where(Ontology.domain.in_(domains))  # noqa
        )

        return result.scalars().all()

    @staticmethod
    async def get_active_ontologies_by_domains(session, domains):
        # type: (AsyncSession, List[str]) -> List[Ontology]
        result = await session.execute(
            select(Ontology).where(
                and_(Ontology.domain.in_(domains), Ontology.active == True)  # noqa
            )
        )
        return result.scalars().all()

    @staticmethod
    async def get_ontology_by_id(session, ontology_id):
        # type: (AsyncSession, Union[str, int]) -> Optional[Ontology]
        result = await session.execute(
            select(Ontology).where(Ontology.id == int(ontology_id))
        )
        return result.scalars().first()

    @staticmethod
    async def update_ontology_active_property(session, ontology_id, active_flag):
        # type: (AsyncSession, Union[str, int], bool) -> None
        query = update(Ontology, values={Ontology.active: active_flag}).where(
            Ontology.id == int(ontology_id)
        )
        query.execution_options(synchronize_session="fetch")
        await session.execute(query)
