from .ontology_entity_transaction import OntologyEntityTransaction
from .ontology_transaction import OntologyTransaction

__all__ = ["OntologyEntityTransaction", "OntologyTransaction"]
