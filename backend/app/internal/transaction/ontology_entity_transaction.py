from typing import final, List, Optional

from sqlalchemy import select, update, and_
from sqlalchemy.ext.asyncio import AsyncSession

from app.internal.models.ontology_entity import OntologyEntity


@final
class OntologyEntityTransaction:
    @staticmethod
    async def get_ontology_entities(session):
        # type: (AsyncSession) -> List[OntologyEntity]
        _query = await session.execute(select(OntologyEntity))
        return _query.scalars().all()

    @staticmethod
    async def get_ontology_entities_by_domains(session, domains):
        # type: (AsyncSession, List[str]) -> List[OntologyEntity]
        _query = await session.execute(
            select(OntologyEntity).where(OntologyEntity.domain.in_(domains))  # noqa
        )
        return _query.scalars().all()

    @staticmethod
    async def get_ontology_entities_by_domain_and_active(
        session, domain, active=True, max_depth=None
    ):
        # type: (AsyncSession, str, bool, Optional[int]) -> List[OntologyEntity]
        results = None
        if max_depth is not None:
            _query = await session.execute(
                select(OntologyEntity).where(
                    and_(
                        OntologyEntity.domain == domain,  # noqa
                        OntologyEntity.active == active,  # noqa
                        OntologyEntity.depth <= max_depth,  # noqa
                    )
                )
            )
            results = _query.scalars().all()
        else:
            _query = await session.execute(
                select(OntologyEntity).where(
                    and_(
                        OntologyEntity.domain == domain,  # noqa
                        OntologyEntity.active == active,  # noqa
                    )
                )
            )
            results = _query.scalars().all()
        return results

    @staticmethod
    async def update_ontology_entity_preprocessed_property(
        session: AsyncSession, ontology_entity_id: str, preprocessed_value: str
    ) -> None:
        query = update(
            OntologyEntity, values={OntologyEntity.preprocessed: preprocessed_value}
        ).where(OntologyEntity.id == ontology_entity_id)
        query.execution_options(synchronize_session="fetch")
        await session.execute(query)

    @staticmethod
    async def update_ontology_entity_active_property(
        session: AsyncSession, ontology_id: int, active_flag: bool
    ) -> None:
        query = update(
            OntologyEntity, values={OntologyEntity.active: active_flag}
        ).where(OntologyEntity.ontology_id == ontology_id)
        query.execution_options(synchronize_session="fetch")
        await session.execute(query)
