import os

from fastapi import APIRouter
from fastapi.staticfiles import StaticFiles
from fastapi import Request
from fastapi.responses import RedirectResponse, FileResponse

from app.dependencies import STATIC_ROOT, SERVE_STATIC_FILES

router = APIRouter()

if SERVE_STATIC_FILES == "true":
    router.mount(
        "/forwot/css",
        StaticFiles(directory=os.path.join(STATIC_ROOT, "css"), check_dir=True),
        name="css",
    )
    router.mount(
        "/forwot/js",
        StaticFiles(directory=os.path.join(STATIC_ROOT, "js"), check_dir=True),
        name="js",
    )
    router.mount(
        "/forwot/fonts",
        StaticFiles(directory=os.path.join(STATIC_ROOT, "fonts"), check_dir=True),
        name="fonts",
    )
    router.mount(
        "/forwot/img",
        StaticFiles(directory=os.path.join(STATIC_ROOT, "img"), check_dir=True),
        name="img",
    )


@router.get("/", response_class=FileResponse)
async def serve_root() -> RedirectResponse:
    """Let's only serve everything under '/forwot"""
    return RedirectResponse(url="/forwot")


@router.get("/forwot", response_class=FileResponse)
async def serve_index() -> FileResponse:
    """Send our index.html"""
    path = f"{STATIC_ROOT}/index.html"
    return FileResponse(path)


@router.get("/{catchall:path}", response_class=FileResponse)
async def catch_rest(request: Request) -> FileResponse:
    """Handle other requests not caught above (api router, root, index)."""
    # check first if requested file exists
    path = request.path_params["catchall"]
    if path.startswith("forwot/"):
        path = path[6:]
    file = STATIC_ROOT + path
    if os.path.isfile(file):
        return FileResponse(file)

    # otherwise return index files
    index = f"{STATIC_ROOT}/index.html"
    return FileResponse(index)
