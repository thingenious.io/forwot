from typing import List, Any
from fastapi import APIRouter, Depends, Query, Path, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession  # noqa
from app.internal.service import (
    OntologyService,
    SearchService,
    WordCloudService,
)  # noqa
from app.dependencies import (
    get_session,
    DEFAULT_BATCH_SIZE,
    DEFAULT_COSINE_THRESHOLD,
    DEFAULT_TRANSFORMER_THRESHOLD,
)

router = APIRouter(prefix="/forwot/api")


@router.get("/ontologies")
async def fetch_ontologies(
    session: AsyncSession = Depends(get_session), domains: List[str] = Query(None)
) -> Any:
    try:
        if domains:
            ontology_list = await OntologyService.get_ontologies_by_domains_in_list(
                session, domains
            )
            results = list(ontology_list)
            await session.close()
            return results
        else:
            ontology_list = await OntologyService.get_ontologies_in_list(session)
            await session.close()
            return ontology_list
    except Exception as ex:
        print(ex)
        raise HTTPException(status_code=500, detail="Error in backend")


@router.get("/ontologies/domains")
async def fetch_ontologies_domains(session: AsyncSession = Depends(get_session)) -> Any:
    try:
        domain_list = await OntologyService.get_unique_domains(session)
        await session.close()
        return domain_list
    except Exception as ex:
        print(ex)
        raise HTTPException(status_code=500, detail="Error in backend")


@router.post("/ontologies/{ontology_id}/blacklist")
async def blacklist_ontology(
    session: AsyncSession = Depends(get_session), ontology_id: str = Path(...)
) -> Any:
    try:
        await OntologyService.blacklist_ontology(session, int(ontology_id))
        await session.commit()
        await session.close()
        return "Ontology successfully blacklisted"
    except Exception as ex:
        print(ex)
        raise HTTPException(status_code=500, detail="Error in backend")


@router.post("/ontologies/{ontology_id}/whitelist")
async def whitelist_ontology(
    session: AsyncSession = Depends(get_session), ontology_id: str = Path(...)
) -> Any:
    try:
        await OntologyService.whitelist_ontology(session, int(ontology_id))
        await session.commit()
        await session.close()
        return "Ontology successfully whitelisted"
    except Exception as ex:
        print(ex)
        raise HTTPException(status_code=500, detail="Error in backend")


@router.get("/wordcloud")
async def get_wordcloud(
    session: AsyncSession = Depends(get_session),
    domains: List[str] = Query(...),
    depth: int = Query(2, ge=0),
) -> Any:
    try:
        if domains:
            image = await WordCloudService.create_wordcloud(
                session, domains, depth=depth
            )
            await session.close()
            return image
        else:
            raise HTTPException(status_code=400, detail="Domains not specified")
    except Exception as ex:
        print(ex)
        raise HTTPException(status_code=500, detail="Error in backend")


@router.get("/stats")
async def get_wordcloud_stats(
    session: AsyncSession = Depends(get_session),
    domains: List[str] = Query(...),
    depth: int = Query(2, ge=0),  # noqa, TODO
) -> Any:
    try:
        if domains:
            stats = await WordCloudService.get_wordcloud_stats(
                session, domains, depth=depth
            )
            await session.close()
            return stats
        else:
            raise HTTPException(status_code=400, detail="Domains not specified")
    except Exception as ex:
        print(ex)
        raise HTTPException(status_code=500, detail="Error in backend")


@router.get("/search-similarity")
async def search_similarity(
    session: AsyncSession = Depends(get_session),
    search_term: str = Query(...),
    ontology_domain: str = Query(...),
    terms: int = Query(DEFAULT_BATCH_SIZE, ge=0),
    alpha: float = Query(DEFAULT_COSINE_THRESHOLD, ge=0),
    beta: float = Query(DEFAULT_TRANSFORMER_THRESHOLD, ge=0),
    depth: int = Query(2, ge=0),  # noqa, TODO
) -> Any:
    try:
        similarity = await SearchService.get_search_similarity(
            session, search_term, ontology_domain, n_terms=terms, alpha=alpha, beta=beta
        )
        await session.close()
        return similarity
    except Exception as ex:
        print(ex)
        raise HTTPException(status_code=500, detail="Error in backend")


@router.get("/search-term")
async def search_term_in_domain(
    session: AsyncSession = Depends(get_session),
    search_term: str = Query(...),
    ontology_domain: str = Query(...),
    depth: int = Query(2, ge=0),  # noqa, TODO
) -> Any:
    try:
        search_term_result = await SearchService.search_term_in_domain(
            session, search_term, ontology_domain
        )
        await session.close()
        return search_term_result
    except Exception as ex:
        print(ex)
        raise HTTPException(status_code=500, detail="Error in backend")
