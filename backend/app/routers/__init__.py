from .forwot_router import router as forwot_router
from .static_files import router as static_files

__all__ = ["forwot_router", "static_files"]
