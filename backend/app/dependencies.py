# Dependency
import os
import json
import httpx
from base64 import b64encode
from typing import AsyncGenerator, Optional, Union, List, Any, Dict

import requests
import numpy as np
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

# similarity thresholds and batch sizes:
# use env vars, so we can easily change them in our yaml files if needed
DEFAULT_BATCH_SIZE = int(os.environ.get("DEFAULT_BATCH_SIZE", "32"))
DEFAULT_COSINE_THRESHOLD = float(os.environ.get("DEFAULT_COSINE_THRESHOLD", "0.5"))
DEFAULT_TRANSFORMER_THRESHOLD = float(os.environ.get("DEFAULT_TRANSFORMER_THRESHOLD", "0.3"))
DEFAULT_CONVERT_TERM_THRESHOLD = float(os.environ.get("DEFAULT_CONVERT_TERM_THRESHOLD", "0.95"))
TORCHSERVE_HOST = os.environ.get("TORCHSERVE_HOST", "http://localhost")
TORCHSERVE_PORT = os.environ.get("TORCHSERVE_PORT", "8080")
SERVE_STATIC_FILES = os.environ.get("SERVE_STATIC_FILES", "false")
STATIC_ROOT = os.path.realpath(os.path.join(os.path.dirname(__file__), "static"))
DB_HOST = os.environ.get("DB_HOST", "localhost")
DB_NAME = os.environ.get("DB_NAME", "ai4eu-db")
DB_PORT = os.environ.get("DB_PORT", "49154")
DB_USER = os.environ.get("DB_USER", "postgres")
DB_PASSWORD = os.environ.get("DB_PASSWORD", "12345678")
DATABASE_URL = (
    f"postgresql+asyncpg://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
)
EMBEDDINGS_DIR = os.environ.get(
    "EMBEDDINGS_DIR",
    os.path.realpath(
        os.path.join(os.path.dirname(__file__),  "..", "embeddings")
    ),
)
engine = create_async_engine(
    DATABASE_URL,
    echo=True,
    future=True,
    pool_size=20,
    max_overflow=2
)
async_session = sessionmaker(bind=engine, class_=AsyncSession, expire_on_commit=True)


async def get_session():
    # type: () -> AsyncGenerator
    """
    Dependency function that yields db sessions
    """
    session: Optional[AsyncSession] = None
    try:
        session = async_session()
        yield session
    finally:
        if session is not None:
            await session.close()


async def torchserve_request(data, embeddings=True):
    # type: (Union[Dict[str, Any], List[List[str]]], bool) -> Optional[np.ndarray]
    """Make a request to torchserve and get embeddings or similarities.

    Parameters
    ----------
    embeddings: request for embeddings or for similarities
    data: Union[Dict[str, Any], List[List[str]]]: the request data to send:

    Returns Optional[np.ndarray]: the torchserve output: embeddings or similarities
    -------

    """
    request_path = (
        "explanations" if embeddings else "predictions"
    )
    url = f"{TORCHSERVE_HOST}:{TORCHSERVE_PORT}/{request_path}/textSimilarity"
    b64_word = b64encode(json.dumps(data).encode()).decode()
    headers = {"Content-Type": "application/json"}
    body = {"instances": [{"data": {"b64": b64_word}}]}
    try:
        async with httpx.AsyncClient() as client:
            response = await client.post(
                url=url, content=json.dumps(body), headers=headers, timeout=1200
            )
            if response.status_code == 200:
                _predictions = response.json().get("predictions", [])
                if not _predictions:
                    return None
                if embeddings:
                    return np.array(_predictions[0])
                return _predictions[0]
    except (requests.HTTPError, Exception) as error:
        print(f"torchserve_request(): {error}")
    return None
