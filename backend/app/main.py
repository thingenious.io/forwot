from typing import Any

import asyncio
import nltk  # noqa
import orjson
from fastapi import FastAPI
from fastapi.responses import ORJSONResponse

from app.internal import models  # noqa
from app.routers import forwot_router as rooter, static_files  # noqa
from app.dependencies import engine, SERVE_STATIC_FILES  # noqa


class CustomORJSONResponse(ORJSONResponse):
    media_type = "application/json"

    def render(self, content: Any) -> bytes:
        return orjson.dumps(
            content, option=orjson.OPT_NON_STR_KEYS | orjson.OPT_SERIALIZE_NUMPY
        )


app = FastAPI(default_response_class=CustomORJSONResponse)
app.include_router(rooter)

if SERVE_STATIC_FILES == "true":
    app.include_router(static_files)


async def create_metadata():
    max_retries = 10
    delay = 5
    current = 0
    while current < max_retries:
        try:
            async with engine.begin() as connection:
                await connection.run_sync(models.Base.metadata.create_all)
                break
        except (ConnectionRefusedError, Exception) as error:
            print(f"Attempt {current + 1} of {max_retries}")
            if current == max_retries - 1:
                raise error
            await asyncio.sleep(delay)
        finally:
            current += 1


@app.head("/health", include_in_schema=False, status_code=204)
async def head_check():  # pragma: no cover
    # type: () -> None
    """Health check (docker)."""
    return None


@app.on_event("startup")
async def startup():
    # type: () -> None
    try:
        nltk.download("stopwords")
        nltk.download("punkt")
    except (FileExistsError, Exception):
        pass

    await create_metadata()
