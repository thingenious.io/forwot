import Vue from "vue";
import Vuetify from "vuetify";
import Router from "vue-router";

Vue.use(Router);
Vue.use(Vuetify);
