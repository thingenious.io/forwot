user  nginx;
worker_processes  auto;
error_log /dev/stdout debug;
pid        /var/run/nginx.pid;
events {
    worker_connections  2048;
}
http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;
    log_format   '$remote_addr - $remote_user [$time_local] "$request" '
                    '$status $body_bytes_sent "$http_referer" '
                    '"$http_user_agent" "$http_x_forwarded_for"';
    access_log /dev/stdout;
    server_tokens off;
    autoindex off;
    keepalive_timeout  65;
    map $http_upgrade $connection_upgrade {
        default upgrade;
        ''      close;
    }

    server {
        listen       8062 default_server;
        listen       [::]:8062 default_server;
        root   /app;
        index  index.html;
        server_name  _;
        location = / {
            return 301 /forwot/;
        }
        location /healthcheck {
            keepalive_timeout 0;
            return 200 'ok';
        }
        location /forwot/api {
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection $connection_upgrade;
            proxy_pass http://localhost:6080;
            proxy_set_header X-Url-Scheme $scheme;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_redirect off;
            proxy_ignore_client_abort on;
            proxy_set_header Host $host;
            proxy_set_header Proxy "";
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-Host $server_name;
            proxy_read_timeout 600;
            proxy_connect_timeout 600;
            proxy_send_timeout 600;
            proxy_buffering on;
        }
        location /forwot {
            alias   /app;
            try_files $uri $uri/ /index.html;
        }
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   /usr/share/nginx/html;
        }
    }
}
