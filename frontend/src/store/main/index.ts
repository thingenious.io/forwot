import { mutations } from "./mutations";
import { actions } from "./actions";
import { getters } from "./getters";
import { MainState } from "@/interfaces";

const defaultState: MainState = {
  dashboardMiniDrawer: false,
  dashboardShowDrawer: true,
  notifications: [],
  ontologies: [],
  domains: null,
};

export const mainModule = {
  state: defaultState,
  mutations,
  actions,
  getters,
};
