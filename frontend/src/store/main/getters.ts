import { State } from "./state";
import { getStoreAccessors } from "typesafe-vuex";
import { AppNotification, MainState, Ontology } from "@/interfaces";

export const getters = {
  dashboardShowDrawer: (state: MainState): boolean => state.dashboardShowDrawer,
  dashboardMiniDrawer: (state: MainState): boolean => state.dashboardMiniDrawer,
  firstNotification: (state: MainState): AppNotification | false =>
    state.notifications.length > 0 && state.notifications[0],
  ontologies: (state: MainState): Ontology[] => state.ontologies,
  domains: (state: MainState): string[] | null => state.domains,
};

const { read } = getStoreAccessors<MainState, State>("");

export const readDashboardMiniDrawer = read(getters.dashboardMiniDrawer);
export const readDashboardShowDrawer = read(getters.dashboardShowDrawer);
export const readFirstNotification = read(getters.firstNotification);
export const readOntologies = read(getters.ontologies);
export const readDomains = read(getters.domains);
