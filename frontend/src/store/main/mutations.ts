import { AppNotification, MainState, Ontology } from "@/interfaces";
import { getStoreAccessors } from "typesafe-vuex";
import { State } from "./state";

export const mutations = {
  setOntologies(state: MainState, payload: Ontology[]): void {
    state.ontologies = payload;
  },
  setDomains(state: MainState, payload: string[]): void {
    state.domains = payload;
  },
  setDashboardMiniDrawer(state: MainState, payload: boolean): void {
    state.dashboardMiniDrawer = payload;
  },
  setDashboardShowDrawer(state: MainState, payload: boolean): void {
    state.dashboardShowDrawer = payload;
  },
  addNotification(state: MainState, payload: AppNotification): void {
    state.notifications.push(payload);
  },
  removeNotification(state: MainState, payload: AppNotification): void {
    state.notifications = state.notifications.filter(
      (notification: AppNotification) => notification !== payload
    );
  },
};

const { commit } = getStoreAccessors<MainState, State>("");

export const commitSetDashboardMiniDrawer = commit(
  mutations.setDashboardMiniDrawer
);
export const commitSetDashboardShowDrawer = commit(
  mutations.setDashboardShowDrawer
);
export const commitAddNotification = commit(mutations.addNotification);
export const commitRemoveNotification = commit(mutations.removeNotification);
export const commitSetOntologies = commit(mutations.setOntologies);
export const commitSetDomains = commit(mutations.setDomains);
