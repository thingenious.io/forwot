import { AppNotification } from "./AppNotification";
import { Ontology } from "@/interfaces/Ontology";

export interface MainState {
  dashboardMiniDrawer: boolean;
  dashboardShowDrawer: boolean;
  ontologies: Ontology[];
  domains: string[] | null;
  notifications: AppNotification[];
}
