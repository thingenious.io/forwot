export interface SimpleSearchResponse {
  entities: { [key: number]: string };
  ontologies: { [key: number]: string };
}
export interface ExtendedSearchResponse {
  ontologies: { [key: number]: string };
  terms: { [key: number]: string };
  cosine_similarity: { [key: number]: number };
  transformer_similarity: { [key: number]: number };
}
export interface SimpleSearchResult {
  entity: string;
  ontology: string;
}
export interface ExtendedSearchResult {
  ontology: string;
  term: string;
  cosineSimilarity: number;
  transformerSimilarity: number;
}
