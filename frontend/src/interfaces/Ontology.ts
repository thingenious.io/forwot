export interface Ontology {
  id: number;
  domain: string;
  name: string;
  status: string;
  url: string;
  active: boolean;
}
