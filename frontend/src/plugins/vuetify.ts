import Vue from "vue";
import Vuetify from "vuetify/lib/framework";
import "@mdi/font/css/materialdesignicons.css";

Vue.use(Vuetify);

let storedTheme: string | null = window.localStorage.getItem("theme");

if (
  storedTheme === null ||
  (storedTheme !== "dark" && storedTheme !== "light")
) {
  storedTheme = "light";
  if (
    window.matchMedia &&
    window.matchMedia("(prefers-color-scheme: dark)").matches
  ) {
    storedTheme = "dark";
    if (!window.localStorage.getItem("theme")) {
      window.localStorage.setItem("theme", "dark");
    }
  }
}
const dark = storedTheme !== null && storedTheme === "dark";

const vuetify = new Vuetify({
  icons: {
    iconfont: "mdi",
  },
  theme: {
    dark,
    options: {
      themeCache: {
        get: (key) => localStorage.getItem(JSON.stringify(key)),
        set: (key, value) => localStorage.setItem(JSON.stringify(key), value),
      },
    },
  },
});

export default vuetify;
