<div align="center">

![image](docs/images/thingenious.png)

# forWoT

</div>

ForWoT is an application for ontology analysis, funded by [AI4EU](https://www.ai4eu.eu/) and developed
by [thingenious.io](https://thingenious.io/). ForWoT offers the following features:

- Wordcloud generation
- Word Similarity
- Stats
- Ontologies Management

Team Members:

| Name                 | Role        | Email
|:----:                |:----:       |:----:
|Panagiotis Kasnesis   | ML Engineer | pkasnesis@uniwa.gr
|Evangelos Katsadouros | SW Engineer | katsadouros.v@uniwa.gr
|Lazaros Toumanidis    | SW Developer | laztoum@uniwa.gr

## Table of Contents

* [Technologies](#technologies)
* [Architecture](#architecture)
* [Setup](#setup)

## Technologies

This project built using the following technologies:

- Python version 3.9
- Postgres DB version 12.2
- Docker
- Kubernetes
- TorchServe

## Architecture

<div style="text-align: justify">

Forwot is a system separated into three logical layers. The first layer is the presentation layer in which runs the
frontend application for better user experience (UX). The second layer is the business-logic layer which contains all
the functionality of the forWoT system. Internally this layer is a small-scale microservice architecture, with a shared
database, in which microservices communicate between them to fulfill their tasks. The last logical layer is the storage
layer which contains the postgres database. The database is communicated only by the forWoT system for storing
ontologies and ontologies entities. Also, cron job which works as a supportive mechanism to forWoT system, communicates
the database for updating and processing the stored ontologies entities.

<div align="center">
![image](docs/images/architecture.jpg)
</div>

The forWoT system (backend) offers two interfaces for communication. The first is the gRPC a high-performance remote
procedure protocol(RPC) and the second one is an HTTP API. Finally, as Machine Learning applications tend to be
heavy-computing applications we use Kubernetes in order for forWoT to scale up offering better user experience and
reliability.

</div>

## Setup
<div style="text-align: justify">
You can use either [docker-compose](https://docs.docker.com/compose) or [Kubernetes](https://kubernetes.io/) in order to deploy the whole solution. A gpu accelerated host is highly recommened (required for kubernetes) for the TorchServe service.

For a kubernetes deployment (asuming that a default [StorageClass](https://kubernetes.io/docs/concepts/storage/storage-classes/) exists on your cluster, and the nvidiadriver), you can either manually `kubectl apply` the provided [./manifests](./manifests) or use the provided deployment [script](./deploy.sh):
```bash
curl -L https://gitlab.com/thingenious.io/forwot/-/raw/master/deploy.sh | bash
```

For docker-compose:
```bash
docker-compose up -d
```
or using a specific tag:
```bash
TAG=v0.0.3 docker-compose up -d
```
If the GPU is not detected on the torchserve container [(issue)](https://github.com/NVIDIA/nvidia-docker/issues/1447#issuecomment-851039827) you can try the additional yml file:
```bash
docker-compose -f docker-compose.nvidia.yml up -d
```
</div>
