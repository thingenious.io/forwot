"""-*- coding: utf-8 -*-."""
import os
import json
from typing import List, Any, Dict, Optional
from abc import ABC

import pandas as pd  # type: ignore # noqa
import numpy as np  # type: ignore # noqa
import torch  # noqa
from ts.context import Context  # type: ignore # noqa
from ts.torch_handler.base_handler import BaseHandler  # type: ignore # noqa
from sentence_transformers import SentenceTransformer  # noqa
from sklearn.metrics.pairwise import cosine_similarity  # noqa
from simpletransformers.classification import ClassificationModel  # type: ignore # noqa

EMBEDDINGS_DIR = os.environ.get("EMBEDDINGS_DIR", "embeddings")


class ForWotSimilaritiesHandler(BaseHandler, ABC):
    """
    Text similarities handler class.
    """

    def __init__(self):
        # type: () -> None
        super(ForWotSimilaritiesHandler, self).__init__()
        os.makedirs("cache_dir", exist_ok=True)
        self.tokenizer = None  # type: Optional[SentenceTransformer]
        self.model = None  # type: Optional[ClassificationModel]
        self.manifest = None  # type: Any
        self.device = None  # type: Optional[torch.device]
        self.initialized = False

    def initialize(self, ctx):
        # type: (Context) -> None
        self.manifest = ctx.manifest

        properties = ctx.system_properties
        model_dir = properties.get("model_dir")
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.model = ClassificationModel(
            "roberta",
            os.path.realpath(model_dir),
            num_labels=1,
            use_cuda=torch.cuda.is_available(),
            args={"silent": True, "use_multiprocessing": False},
        )
        self.tokenizer = SentenceTransformer("roberta-large-nli-stsb-mean-tokens")
        self.initialized = True

    def preprocess(self, data):
        # type: (List[Dict[str, Any]]) ->Dict[str,Any]
        text = data[0].get("data", None)
        if text is None:
            text = data[0].get("body", "")
        return json.loads(text)

    def postprocess(self, data):
        # type: (list[Any]) -> List[Any]
        return data

    def _domain_embeddings(self, domain_name, domain_df, category, depth=None, force=True):
        file_name = f"{domain_name}_{category}.npy"
        if depth is not None and depth > 0:
            file_name = f"{domain_name}_{category}_depth{depth}.npy"
        file_path = os.path.realpath(os.path.join(EMBEDDINGS_DIR, file_name))
        if os.path.isfile(file_path) and not force:
            return np.load(file_path)
        data = list(domain_df[category].apply(str))
        _embeddings = self.tokenizer.encode(data)
        np.save(file_path, _embeddings)
        return _embeddings

    def inference(self, data, *args, **kwargs):
        # type: (Dict[str, Any], Context, Any) ->List[Any]
        domain_name = data.get("domain_name")
        text = data.get("text")
        domain_df = pd.read_json(data.get("domain_df"))
        ontologies_df = pd.read_json(data.get("ontologies_df"))
        n_terms = data.get("n_terms")
        alpha = data.get("alpha")
        beta = data.get("beta")
        gamma = data.get("gamma")
        force = data.get("force", True)
        print(
            {
                "domain_name": domain_name,
                "text": text,
                "n_terms": n_terms,
                "alpha": alpha,
                "beta": beta,
                "gamma": gamma,
                "force": force,
                "ontologies_df.shape": ontologies_df.shape,
                "domain_df.shape": domain_df.shape,
            }
        )
        entity_embeddings_domain = self._domain_embeddings(
            domain_df=domain_df, domain_name=domain_name, category="preprocessed", force=force
        )
        converted_term = self.convert_search_term(
            entity_embeddings_domain=entity_embeddings_domain,
            text=text,
            domain_df=domain_df,
            ontologies_df=ontologies_df,
            n_terms=n_terms,
            gamma=gamma,
        )
        description_embeddings = self._domain_embeddings(
            domain_name=domain_name, domain_df=domain_df, category="description", force=force
        )
        print(description_embeddings.shape)
        return self.search_similar_entity(
            text=converted_term,
            domain_df=domain_df,
            ontologies_df=ontologies_df,
            n_terms=n_terms,
            alpha=alpha,
            beta=beta,
            entity_embeddings_domain=entity_embeddings_domain,
            description_embeddings=description_embeddings,
        )

    def convert_search_term(
        self, text, domain_df, ontologies_df, n_terms, gamma, entity_embeddings_domain
    ):
        """
        text: the entity the user searches for
        domain_df:
        ontologies_df
        n_terms
        gamma: cosine similarity threshold
        entity_embeddings_domain

        Outputs
        search_term:converted or not
        """
        # get entity's embeddings
        search_term = None
        text = text.lower()
        new_emb = self.tokenizer.encode([text])
        entities_df = pd.DataFrame(
            {
                "ontologies": [],
                "terms": [],
                "cosine_similarity": [],
            }
        )
        # iterate over the existing ontologies
        for i in domain_df["ontology_id"].unique():
            indices = domain_df.index[domain_df["ontology_id"] == i]
            cos_sim = cosine_similarity(new_emb, entity_embeddings_domain[indices])
            high_txt = np.argsort(cos_sim)[0, -n_terms:]
            sim_txt = cos_sim[0, high_txt]
            actual_terms = domain_df["ontology_class"].iloc[indices].iloc[high_txt]
            onts = ontologies_df["url"][ontologies_df["id"] == i].values[0]
            results = pd.DataFrame(
                {
                    "ontologies": onts,
                    "terms": actual_terms,
                    "cosine_similarity": sim_txt,
                }
            )
            matched = results[results["cosine_similarity"] >= gamma]
            if len(matched) > 0:
                entities_df = entities_df.append(matched, ignore_index=True)
        if len(entities_df) > 0:
            most_close_term = entities_df.iloc[
                entities_df["cosine_similarity"].argmax()
            ]["terms"]
            if len(most_close_term) > 0:
                search_term = domain_df[
                    domain_df["ontology_class"]
                    == entities_df.iloc[entities_df["cosine_similarity"].argmax()][
                        "terms"
                    ]
                ]["description"].iloc[0]

        if not search_term:
            search_term = text
        return search_term

    def search_similar_entity(
        self,
        text,
        domain_df,
        ontologies_df,
        n_terms,
        alpha,
        beta,
        entity_embeddings_domain,
        description_embeddings,
    ):
        """
        text: the entity the user searches for
        domain_df:
        ontologies_df
        n_terms
        alpha: cosine similarity threshold
        beta: roberta similarity threshold
        entity_embeddings_domain

        Outputs

        """
        # get entity's embeddings
        new_emb = self.tokenizer.encode([text])
        entities_df = pd.DataFrame(
            {
                "ontologies": [],
                "terms": [],
                "cosine_similarity": [],
                "transformer_similarity": [],
            }
        )
        text = text.lower()

        # iterate over the existing ontologies
        for i in domain_df["ontology_id"].unique():
            # print(f"similarities_handler: {i}")
            indices = domain_df.index[domain_df["ontology_id"] == i]
            # entity similarity
            cos_sim = cosine_similarity(new_emb, entity_embeddings_domain[indices])
            high_txt = np.argsort(cos_sim)[0, -n_terms:]
            sim_txt = cos_sim[0, high_txt]
            df_cos_sim = pd.DataFrame({"ind": high_txt, "similarity": sim_txt})
            # description similarity
            cos_sim_descr = cosine_similarity(new_emb, description_embeddings[indices])
            high_txt_descr = np.argsort(cos_sim_descr)[0, -n_terms:]
            sim_txt_descr = cos_sim_descr[0, high_txt_descr]
            df_cos_sim_descr = pd.DataFrame(
                {"ind": high_txt_descr, "similarity": sim_txt_descr}
            )
            # merge and pick only the unique first n_terms
            df_combined_cos_sim = (
                pd.concat([df_cos_sim, df_cos_sim_descr])
                .groupby(["ind"])
                .max()
                .sort_values(["similarity"])
            )
            high_txt = df_combined_cos_sim.index.values
            sim_txt = df_combined_cos_sim.similarity.values

            actual_terms = domain_df["ontology_class"].iloc[indices].iloc[high_txt]
            preproc_terms = domain_df["preprocessed"].iloc[indices].iloc[high_txt]
            onts = ontologies_df["url"][ontologies_df["id"] == i].values[0]

            batch = []
            for n in range(len(actual_terms)):
                batch.append([text, list(preproc_terms)[n]])
            results = pd.DataFrame(
                {
                    "ontologies": onts,
                    "terms": actual_terms,
                    "cosine_similarity": sim_txt,
                    "transformer_similarity": self.model.predict(batch)[0] / 5,
                }
            )
            matched = results[
                (
                    (results["transformer_similarity"] >= beta)
                    | (results["cosine_similarity"] >= alpha)
                )
                & (
                    (results["transformer_similarity"] >= 0.2)
                    & (results["cosine_similarity"] > 0.2)
                )
            ]
            if len(matched) > 0:
                entities_df = entities_df.append(matched, ignore_index=True)
        print(len(entities_df))
        print(entities_df.head())
        return [entities_df.to_json()]

    def handle(self, data, context):
        # type: (List[Dict[str, Any]], Context) ->List[Any]
        results = super(ForWotSimilaritiesHandler, self).handle(data, context)
        if not isinstance(results, list):
            results = results.tolist()  # type: ignore
        return results

    def explain_handle(self, data_preprocess, raw_data):
        # type: (Dict[str, Any], List[Dict[str, Any]]) -> Any
        domain_name = data_preprocess.get("domain_name")
        terms = data_preprocess.get("terms", [])
        depth = data_preprocess.get("depth", None)
        embeddings = self.tokenizer.encode(terms)  # type: ignore
        print(f"[torchserve] embeddings: depth: {depth}")
        file_path = os.path.realpath(
            os.path.join(EMBEDDINGS_DIR, f"{domain_name}_preprocessed.npy")
        )
        if depth is not None:
            file_path = os.path.realpath(
                os.path.join(EMBEDDINGS_DIR, f"{domain_name}_preprocessed_depth{depth}.npy")
            )
        np.save(file_path, embeddings)
        if not isinstance(embeddings, list):
            embeddings = embeddings.tolist()  # type: ignore
        return [embeddings]
