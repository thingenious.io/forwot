FROM pytorch/torchserve:latest-gpu

USER root
RUN apt update && \
    apt install -y wget unzip && \
    rm -rf /var/lin/apt/lists/*

COPY ./requirements.txt /tmp/
RUN pip install -r /tmp/requirements.txt
RUN printf "\nservice_envelope=json" >> /home/model-server/config.properties

USER model-server
WORKDIR  /home/model-server/

# our model zip archive
ARG ROBERTA_URL=https://snf-3035.ok-kno.grnetcloud.net/best_roberta_stsb.zip
ENV ROBERTA_URL=$ROBERTA_URL
RUN wget --quiet $ROBERTA_URL && \
    echo "5a790d0112717183716a9f8c1282df4d73bbde6da5ba1127513ce59ce4ce03bd" best_roberta_stsb.zip | sha256sum -c

RUN unzip best_roberta_stsb.zip && \
    mkdir -p /home/model-server/model_store && \
    mv ./best_roberta_stsb /home/model-server/model_store/best_roberta_stsb && \
    rm best_roberta_stsb.zip

# early download of "roberta-large-nli-stsb-mean-tokens"
RUN python -c "from sentence_transformers import SentenceTransformer;SentenceTransformer(model_name_or_path='roberta-large-nli-stsb-mean-tokens');"
# torch with cuda 11 (base image has for cuda 10)
RUN pip install torch==1.9.0+cu111 torchvision==0.10.0+cu111 torchaudio==0.9.0 -f https://download.pytorch.org/whl/torch_stable.html
# our custom inference/explanations handler
COPY similarities_handler.py /home/model-server/model_store/similarities_handler.py
# create model archive: "textSimilarity.mar"
RUN cd model_store && \
    torch-model-archiver \
    --model-name textSimilarity \
    --version 1.0 \
    --serialized-file /home/model-server/model_store/best_roberta_stsb/pytorch_model.bin \
    --extra-files "/home/model-server/model_store/best_roberta_stsb/vocab.json,\
/home/model-server/model_store/best_roberta_stsb/config.json,\
/home/model-server/model_store/best_roberta_stsb/merges.txt,\
/home/model-server/model_store/best_roberta_stsb/eval_results.txt,\
/home/model-server/model_store/best_roberta_stsb/eval_results.txt,\
/home/model-server/model_store/best_roberta_stsb/model_args.json,\
/home/model-server/model_store/best_roberta_stsb/scheduler.pt,\
/home/model-server/model_store/best_roberta_stsb/special_tokens_map.json,\
/home/model-server/model_store/best_roberta_stsb/tokenizer_config.json" \
    --requirements-file /tmp/requirements.txt \
    --handler /home/model-server/model_store/similarities_handler.py
# set config properties for the models
COPY append.properties  /home/model-server/model_store/append.properties
RUN cat /home/model-server/model_store/append.properties \
    /home/model-server/config.properties > /home/model-server/config.properties.swp && \
    mv /home/model-server/config.properties.swp /home/model-server/config.properties
ENV EMBEDDINGS_DIR=/home/model-server/embeddings
RUN mkdir -p $EMBEDDINGS_DIR
CMD ["torchserve", \
    "--start", \
    "--foreground", \
    "--no-config-snapshots", \
    "--ts-config=/home/model-server/config.properties", \
    "--model-store", "/home/model-server/model_store/", \
    "--models", "textSimilarity=textSimilarity.mar" \
    ]
